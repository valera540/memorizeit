#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "login_dialog.h"
#include "createdeck_dialog.h"
#include "training_dialog.h"

const QString HOSTNAME = "localhost";
const QString DATABASE = "MemorizeIt";
const QString USERNAME = "root";
const QString PASSWORD = "";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    uid_ = -1;

    db_ = QSqlDatabase::addDatabase("QMYSQL", "main");

    db_.setHostName(HOSTNAME);
    db_.setDatabaseName(DATABASE);
    db_.setUserName(USERNAME);
    db_.setPassword(PASSWORD);

    if(!db_.open())
    {
        QMessageBox::critical(this,
            tr("MemorizeIt"),
            tr("Connection error!"),
            tr(db_.lastError().text().toStdString().c_str()));

        db_.close();
        exit(1);
    }
    else
        qDebug("Main: Connection successful!");


    Login_Dialog* loginDialog = new Login_Dialog(db_, this);

    if(loginDialog->exec())
    {
        qDebug("Main: login successful!");
        qDebug("Main: user id: " + QString::number(uid_).toLatin1());
    }
    else
    {
        qDebug("Main: login aborted");
        db_.close();
        exit(0);
    }
    loginDialog->close();

    updateList();

    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Front"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Last Training"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Trainings"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Accuracy"));
}

void MainWindow::setUid(int uid)
{
    uid_ = uid;
}

void MainWindow::updateList()
{
    ui->listWidget->clear();

    QSqlQuery query(db_);

    // SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema='MemorizeIt' AND table_name='user[id]_[title]'
    query.exec("SELECT title FROM Decks WHERE uid = " + QString::number(uid_) +";");


    if (query.numRowsAffected() > 0)
    {
        while (query.next())
        {
            QString result = query.value(0).toString();
            ui->listWidget->addItem(result);
        }
    }
}

MainWindow::~MainWindow()
{
    db_.close();
    delete ui;
}

void MainWindow::on_bttn_createDeck_clicked()
{
    CreateDeck_dialog* createDeck_dialog = new CreateDeck_dialog(db_, uid_);

    if (createDeck_dialog->exec())
    {
        qDebug("Main: deck created");
        updateList();
    }
    else
        qDebug("Main: deck creating aborted");
}

void MainWindow::on_bttn_removeDeck_clicked()
{
    QString table = "user" + QString::number(uid_) + "_" + ui->listWidget->currentItem()->text();

    QSqlQuery query(db_);
    query.exec("DROP TABLE " + table);

    query.exec("DELETE FROM Decks WHERE uid=" + QString::number(uid_) + " AND title='" + ui->listWidget->currentItem()->text() + "'");

    updateList();
    ui->tableWidget->setRowCount(0);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
   //ui->tableWidget->clear();
   ui->tableWidget->setRowCount(0);
   QSqlQuery query(db_);

   QString table = "user" + QString::number(uid_) + "_" + item->text();
   query.exec("SELECT front, lastTraining, trainings, successTrainings FROM " + table);

   while (query.next())
   {
       QString front = query.value(0).toString();
       QString lt    = query.value(1).toString().split('T')[0];
       int     tr    = query.value(2).toInt();
       int     st    = query.value(3).toInt();

       ui->tableWidget->insertRow(ui->tableWidget->rowCount());
       ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(front));
       ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, new QTableWidgetItem(lt));
       ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 2, new QTableWidgetItem(QString::number(tr)));

       if (st == 0)
           ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem("0 %"));
       else
           ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 3, new QTableWidgetItem(QString::number(st/tr*100) + " %"));
   }
}

void MainWindow::on_bttn_train_clicked()
{
    training_dialog* training = new training_dialog(db_, uid_, ui->listWidget->currentItem()->text());

    if (training->exec())
    {
        qDebug("Main: training completed");
    }
}
