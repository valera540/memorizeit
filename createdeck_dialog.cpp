#include "createdeck_dialog.h"
#include "ui_createdeck_dialog.h"

CreateDeck_dialog::CreateDeck_dialog(QSqlDatabase db, int uid, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateDeck_dialog)
{
    ui->setupUi(this);

    count_ = 0;
    uid_ = uid;
    db_ = &db;
}

CreateDeck_dialog::~CreateDeck_dialog()
{
    delete ui;
}

void CreateDeck_dialog::on_bttn_add_clicked()
{
    Card card;
    card.front = ui->line_front->text();
    card.back  = ui->line_back->text();


    for(auto elem: list_)
        if (elem.front == card.front)
        {
            QMessageBox::critical(this,
                tr("MemorizeIt"),
                tr("Card with this front already exists!"));
            return;
        }

    list_ << card;
    ui->label_added->setText("Cards added: " + QString::number(++count_));
    ui->line_front->clear();
    ui->line_back->clear();
}

void CreateDeck_dialog::on_bttn_ok_clicked()
{
    if (ui->line_title->text().isEmpty())
    {
        QMessageBox::critical(this,
            tr("MemorizeIt"),
            tr("Title can't be empty!"));
        return;
    }


    const QString table_name = "user" + QString::number(uid_) + "_" + ui->line_title->text();

    QSqlQuery query(*db_);

    query.exec("SELECT CONCAT('user', uid, '_', title) FROM Decks WHERE CONCAT('user', uid, '_', title)='" + table_name + "';");

    if(query.numRowsAffected() != 0)
    {
        qDebug("Deck creating: deck duplicate!:\n");
        QMessageBox::critical(this,
            tr("MemorizeIt"),
            tr("Deck with this title already exists!"));

        return;
    }

    if(query.exec("INSERT INTO Decks VALUES("
                  + QString::number(uid_) +  ", '"
                  + ui->line_title->text() + "', "
                  + "null);"))
        qDebug("Deck creating: Deck created");
    else
    {
        qDebug("Deck creating: deck creating error:\n" + query.lastError().text().toUtf8());
        QMessageBox::critical(this,
            tr("MemorizeIt"),
            tr("Deck creating error!"));

        return;
    }


    query.exec("CREATE TABLE " + table_name + "(uid int, front varchar(50), back varchar(50), "
               "lastTraining datetime, trainings int, successTrainings int)");
    qDebug("Deck creating: Subject created");


    for (auto card: list_)
        query.exec("INSERT INTO " + table_name + " VALUES(" + QString::number(uid_) + ", '" + card.front + "', '" + card.back + "', null, 0, 0)");

    accept();
}

void CreateDeck_dialog::on_bttn_cancel_clicked()
{
    reject();
}
