#ifndef TRAINING_DIALOG_H
#define TRAINING_DIALOG_H

#include <QDialog>
#include <QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QMessageBox>

namespace Ui {
class training_dialog;
}

class training_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit training_dialog(QSqlDatabase db, int uid, QString item, QWidget *parent = nullptr);
    ~training_dialog();

private slots:
    void on_bttn_next_clicked();

    void on_bttn_cancel_clicked();

private:
    QSqlDatabase* db_;
    QSqlQuery* query_;
    float counter_;
    QString table_;
    int uid_;

    Ui::training_dialog *ui;
};

#endif // TRAINING_DIALOG_H
