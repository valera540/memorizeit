#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QListWidgetItem>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setConnection(QSqlDatabase*);
    void setUid(int);
    void updateList();

private slots:
    void on_bttn_createDeck_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_bttn_train_clicked();

    void on_bttn_removeDeck_clicked();

private:
    Ui::MainWindow *ui;
    QSqlDatabase db_;
    int uid_;
};

#endif // MAINWINDOW_H
