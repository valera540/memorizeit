#include "login_dialog.h"
#include "ui_login_dialog.h"

Login_Dialog::Login_Dialog(QSqlDatabase db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login_Dialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Authorization");
    mode_ = 0;
    db_ = &db;
    parent_ = (MainWindow*)parent;
}

Login_Dialog::~Login_Dialog() { delete ui; }

void Login_Dialog::on_tabWidget_currentChanged(int index)
{
    qDebug("Login Dialog: mode changed to " + QString::number(index).toLatin1());
    mode_ = index;
}

void Login_Dialog::on_bttn_exit_clicked()
{
    this->close();
}

void Login_Dialog::on_bttn_confirm_clicked()
{
    switch (mode_)
    {
        case 0:
        {
            qDebug("Login Dialog: Sign In...");

            const QString LOGIN = ui->line_login_SI->text();
            const QString PASSWORD = ui->line_password_SI->text();

            QSqlQuery query(*db_);
            if (query.exec("SELECT id FROM Users WHERE login=\"" + LOGIN + "\" AND password=\"" + PASSWORD + "\""))
            {
                query.next();
                if (query.numRowsAffected() == 0)
                {
                    qDebug("==> Invalid login/password");
                    QMessageBox::critical(this,
                        tr("MemorizeIt"),
                        tr("Invalid login/password!"));
                }
                else
                {
                    parent_->setUid(query.value(0).toInt());
                    accept();
                }
            }

            break;
        }
        case 1:
        {
            qDebug("Login Dialog: Sign Up...");

            const QString NICKNAME  = ui->line_nickname_SU->text();
            const QString LOGIN     = ui->line_login_SU->text();
            const QString PASSWORD1 = ui->line_password_SU->text();
            const QString PASSWORD2 = ui->line_passwordA_SU->text();

            if (PASSWORD1 != PASSWORD2)
            {
                QMessageBox::critical(this,
                    tr("MemorizeIt"),
                    tr("Passwords not same!"));
            }
            else
            {
                QSqlQuery query(*db_);

                if (query.exec("INSERT INTO Users VALUES(NULL, \"" + LOGIN + "\", \"" + PASSWORD1 + "\", \"" + NICKNAME + "\", \"\")"))
                {
                    qDebug("==> User created!");
                    query.exec("SELECT id FROM Users WHERE login=\"" + LOGIN + "\"");
                    query.next();
                    parent_->setUid(query.value(0).toInt());
                    accept();
                }
                else
                    qDebug("==> User not created!");

            }

            break;
        }

    }
}
